from flask import Flask, jsonify, make_response, abort, request
from bson.objectid import ObjectId
app = Flask(__name__)

from mongoengine import connect
from models import Question

from bson import json_util
import json
import os
from helpers.email import *

from flask_cors import CORS, cross_origin
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# connect(host='mongodb://<user>:<pass>@ds149252.mlab.com:49252/<db>')
# connect(host=os.getenv('DB_HOST'))
# print(os.getenv('DB_HOST'))

#mongodb://<user>:<pass>@ds149252.mlab.com:49252/<db>

@app.route('/api/question', methods=['GET'])
def get_all_questions():    
    questions = Question.objects.all()         
    return jsonify(result=json.loads(questions.to_json()))

@app.route('/api/question/<id>', methods=['GET'])
def get_one_question(id):    
    questions = Question.objects(id=ObjectId(id))    
    return jsonify(result=json.loads(questions.to_json()))

@app.route('/api/question', methods=['POST'])
def add_question():
    question = Question(**request.json).save()   
    message = format_email(author=request.json['author'],description=request.json['description'],cpf=request.json['cpf'])
    send_email(message=message)      
    return jsonify(result=json.loads(question.to_json()))

@app.route('/api/question/<id>', methods=['DELETE'])
def remove_question(id):    
    Question.objects(id=ObjectId(id)).delete()    
    return make_response(jsonify({'status': 'ok'}), 200)

@app.route('/api/question/<id>', methods=['PUT'])
def update_question(id):    
    Question.objects(id=ObjectId(id)).update(**request.json)    
    question = Question.objects(id=ObjectId(id))  
    return jsonify(result=json.loads(question.to_json()))

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == '__main__':
    app.run(debug=True)
