# Governo Aberto - Atas

## 09/10/2018
Discussão sobre possíveis soluções para o problema de automatizações para o envio de pedidos de informação à Câmara de Vereadores de São Paulo.

O site da Câmara disponibiliza um formulário para o envio do pedido, porém é necessário passar pelo [reCaptcha](https://www.google.com/recaptcha/intro/v3beta.html) desenvolvida pela Google. Pesquisamos maneiras de burlar ele, porém vimos que era bastante difícil.

Outra alternativa era criarmos um sistema web que basicamente preenchesse automaticamente os campos do formulário e exibisse para o usuário apenas o Captcha por meio de um [iframe](https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/iframe), porém a injeção de código em um iframe por domínios diferentes (nosso sistema e o formulário da Câmara), é bloqueado pelo navegador por motivos de [segurança](https://en.wikipedia.org/wiki/Same-origin_policy).

Descobrimos uma outra alternativa, que era enviando um e-mail diretamente para a Câmara, informando apenas o Nome, CPF e a descrição da solicitação. Optamos então em desenvolver um sistema que recebe essas informações do usuário e o nosso sistema envia para a Câmara o pedido. Com a resposta da Câmara, nós enviaríamos a resposta para o usuário.