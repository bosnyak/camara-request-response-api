import React, { Component } from "react";
import Modal from "react-modal";
import Questions from "./Questions";
import ReactLoading from "react-loading";
import QuestionForm from "./QuestionForm";
import Question from "./Question";
import { API_URL } from "./API";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
  }
};
Modal.setAppElement("#root");
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      modalContent: <h1>AAA</h1>,
      questions: []
    };
  }
  componentDidMount() {
    this.getQuestions();
  }
  getQuestions() {
    this.setState({ modalIsOpen: false });
    this.loaded = false;
    fetch(`${API_URL}/question`)
      .then(res => res.json())
      .then(result => {
        this.loaded = true;
        this.setState({ questions: result.result });
      });
  }
  getQuestion(id) {
    fetch(`${API_URL}/question/${id}`)
      .then(res => res.json())
      .then(result => {
        let question = result.result[0];
        console.log(result);
        this.setState({
          modalIsOpen: true,
          modalContent: (
            <div>
              <h2 ref={subtitle => (this.subtitle = subtitle)}>Pedido</h2>

              <br />
              <Question question={question} disableClick={true} />
              <br />
              <h2 ref={subtitle => (this.subtitle = subtitle)}>Resposta</h2>
              <div>
                {question.answer ? question.answer : "Nenhuma resposta"}
              </div>
            </div>
          )
        });
      });
  }
  newQuestion() {
    this.setState({ modalIsOpen: true });
    this.setState({
      modalIsOpen: true,
      modalContent: (
        <div>
          <h2 ref={subtitle => (this.subtitle = subtitle)}>Novo pedido</h2>

          <br />
          <QuestionForm getQuestions={() => this.getQuestions()} />
        </div>
      )
    });
  }
  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <div className="row">
            <div className="col-md-12">
              <h3>Câmara de Vereadores de São Paulo</h3>
              <h3>Pedidos de informação</h3>
              <hr />
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-md-12">
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Buscar pedido de informação"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <button
                className="btn btn-primary float-right"
                onClick={() => this.newQuestion()}
              >
                Enviar novo pedido
              </button>
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-md-12">
              {this.loaded ? (
                <Questions
                  questions={this.state.questions}
                  questionClick={id => this.getQuestion(id)}
                />
              ) : (
                <ReactLoading
                  className="center"
                  type={"spin"}
                  color={"#1DA1F2"}
                  height={50}
                  width={50}
                />
              )}
            </div>
          </div>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={() => this.setState({ modalIsOpen: false })}
          style={customStyles}
          contentLabel="Example Modal"
        >
          {this.state.modalContent}
        </Modal>
      </div>
    );
  }
}

export default Home;
